<?php
/**
 * Gentium functions and definitions
 *
 * @link       https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package    WordPress
 * @subpackage Gentium
 * @since      1.0.0
 */

if ( ! function_exists( 'pixe_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function pixe_setup(): void {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on components, use a find and replace
		 * to change 'gentium' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'gentium', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'pixe-featured-image', 1024, 420, true );
		add_image_size( 'pixe-standard-large', 980, 735 );
		add_image_size( 'pixe-grid-image', 720, 500, true );
		add_image_size( 'pixe-small-thumb', 100, 90, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			[
				'menu-1'        => esc_html__( 'Primary Menu', 'gentium' ),
				'menu-2'        => esc_html__( 'Mobile Menu (Optional)', 'gentium' ),
				'menu-one-page' => esc_html__( 'One Page Menu', 'gentium' ),
			]
		);

		/**
		 * Add support for core custom logo.
		 */
		add_theme_support(
			'custom-logo',
			[
				'height'      => 200,
				'width'       => 200,
				'flex-width'  => true,
				'flex-height' => true,
			]
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			]
		);

		add_theme_support( 'custom-header' );
		add_editor_style();

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'pixe_custom_background_args',
				[
					'default-color' => 'ffffff',
					'default-image' => '',
				]
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'pixe_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pixe_content_width(): void {
	$GLOBALS['content_width'] = apply_filters( 'pixe_content_width', 1170 );
}

add_action( 'after_setup_theme', 'pixe_content_width', 0 );


if ( ! function_exists( 'wp_body_open' ) ) {
	/**
	 * Fire the wp_body_open action.
	 *
	 * Added for backwards compatibility to support WordPress versions prior to 5.2.0.
	 */
	function wp_body_open(): void {
		/**
		 * Triggered after the opening <body> tag.
		 */
		do_action( 'wp_body_open' );
	}
}

/**
 * Return early if Custom Logos are not available.
 *
 * @todo Remove after WP 4.7
 */
function pixe_the_custom_logo() {
	if ( ! function_exists( 'the_custom_logo' ) ) {
		return;
	}

	the_custom_logo();
}

/**
 * Register widget area.
 *
 * @return void
 */
function pixe_widgets_init(): void {
	register_sidebar(
		[
			'name'          => esc_html__( 'Sidebar', 'gentium' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'This sidebar will be visible on the pages with default template option.', 'gentium' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		]
	);
}

add_action( 'widgets_init', 'pixe_widgets_init' );

/**
 * Add scripts and style.
 *
 * @return void
 */
function pixe_scripts(): void {

	global $wp_query;

	$url     = get_template_directory_uri();
	$version = '1.3.0';

	// All necessary CSS.
	wp_enqueue_style( 'font-awesome', $url . '/assets/icons/font-awesome/css/font-awesome.min.css', '', $version );
	wp_enqueue_style( 'uikit', $url . '/assets/css/uikit.min.css', '', $version );
	wp_enqueue_style( 'pixe-main-style', get_stylesheet_uri(), [] );

	// All Necessary Script.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'anime-js', $url . '/assets/js/anime.min.js', [], '2.2', true );
	wp_enqueue_script( 'jquery-easing', $url . '/assets/js/jquery.easing.js', [], '1.3', true );
	wp_enqueue_script( 'uikit', $url . '/assets/js/uikit.min.js', [], '3.0', true );
	wp_enqueue_script( 'pixe-load-more-script', $url . '/assets/js/load-more.js', [ 'jquery' ], '1.0', true );
	wp_enqueue_script( 'pixe-scripts', $url . '/assets/js/main-script.js', [ 'jquery' ], '1.0', true );

	wp_localize_script(
		'pixe-load-more-script',
		'pixe_loadmore',
		[
			'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php',
			'posts'        => wp_json_encode( $wp_query->query_vars ),
			'current_page' => get_query_var( 'paged' ) ?: 1,
			'max_page'     => $wp_query->max_num_pages,
			'nonce'        => wp_create_nonce( 'loadmore' ),
		]
	);

}

add_action( 'wp_enqueue_scripts', 'pixe_scripts' );

/**
 * Load file.
 */
require get_template_directory() . '/inc/core/theme-core.php';
